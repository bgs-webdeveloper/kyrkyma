$(function () {
    // Slick slider
    $('.js-multiple-slick-slider').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        margin: 10,
        arrows: true,
        dots: false,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    });

    $('.js-tab').on('click', function(){
      var tabIndex = $(this).attr('data-tab-index');

      // Toggle class active
      $('.js-tab').removeClass('active');
      $(this).addClass('active');

      // Toggle tab content
      $('.js-tab-content').addClass('hidden');
      $('.js-tab-content[data-tab-content-index='+ tabIndex +']').removeClass('hidden');
      $(".js-multiple-slick-slider").slick('slickSetOption', 'adaptiveHeight', true, true);
    })

    function loadImageAdvantage () {
      if(window.innerWidth <= 480){
        $('.image-advantage2').attr('src', '../../img/dishes-mobile.png')
        $('.image-bg-certificate').attr('src', '../../img/bg-certeficate-mobile.png')
      }
    }
    loadImageAdvantage();

    $(window).resize(function(){
      loadImageAdvantage()
    });

    var arrHeaderMenu = [];

    $('.header-menu > li > a').each(function(i, item) {
      if( i !== 2) {
        arrHeaderMenu.push($(this).text());
      } else {
        var urlLogo = $(item).find('img').attr('src');
        $('.header-mobile-logo').find('img').attr('src', urlLogo);
      }
    });
    
    // Open header menu
    $('.burger-menu').on('click', function(){
      $('.header-mobile').addClass('active')
    });
    //Close header menu
    $('.js-header-menu-close').on('click', function(){
      $('.header-mobile').removeClass('active')
    })

    // Send data in contact form
    $('.js-btn-form').on('click', function(e){
      e.preventDefault();
      var emailValue = $('.contact-form-valid-email').val();

      if(validateEmail(emailValue)){
        $('body').css('overflow', 'hidden');
        $('.js-modal').toggleClass('hidden');
      }
    });

    // Success send data in contact form
    $('.js-modal-close').on('click', function(){
      $('body').css('overflow', 'auto');
      $('.js-modal').toggleClass('hidden');
    })

    /* фикс отображения картинок для IE */

    var userAgent, ieReg, ie;

    userAgent = window.navigator.userAgent;
    ieReg = /msie|Trident.*rv[ :]*11\./gi;
    ie = ieReg.test(userAgent);

    if(ie) {
      $('.where-can-one-buy__image-background').each(function () {
        var $container = $(this),
        imgUrl = $container.find("img").prop("src");
        if (imgUrl) {
          $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
        }
      });
    };

    function validateEmail(email) {
      var pattern  = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return pattern .test(String(email).toLowerCase());
    }
}());